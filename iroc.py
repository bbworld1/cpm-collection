#!/usr/bin/env python3
# This script finds the derivative/instantaneous rate of change (IROC)
# at a specified point on a function (if it exists)
# using the formal definition of a limit
# Unfortunately only supports plain polynomials at the moment (will expand later)
# yes kai, this is also by myself
import matplotlib.pyplot as plt
import numpy as np
from sympy import *
from sympy.parsing.sympy_parser import parse_expr

# Get inputs
func_text = input("Enter the function (** for power, not ^): ")
xval = sympify(input("Enter the x-value (x to get slope equation): "))

# Set up SymPy symbols
x, h = symbols("x h")

# Parse input
func = parse_expr(func_text)

print("Derivative function:", diff(func))

print()
print("------ Work --------")
print("Definition of the derivative: lim (h->0) ((f(x+h) - f(x)) / h)")

print()
print("Plug in function:")
func_with_h = func.subs(x, x+h)
if xval not in {"", "x"}:
    func_with_h = func_with_h.subs(x, xval)
    func = func.subs(x, xval)
print("lim (h -> 0) ((({}) - ({}))/h)".format(func_with_h, func))

print()
print("Expand function:")
expanded_func_with_h = expand(func_with_h)
expanded_func = expand(func)
print("lim (h -> 0) ((({}) - ({}))/h)".format(expanded_func_with_h, expanded_func))

print()
print("Eliminate terms:")
eliminated_func = expanded_func_with_h - expanded_func
print("lim (h -> 0) (({})/h)".format(eliminated_func))

print()
print("Simplify:")
simplified_func = simplify(eliminated_func) / h
print("lim (h -> 0) ({})".format(simplified_func))

print()
print("Evaluate:")
print("lim (h -> 0) ({}) = {}".format(simplified_func, simplified_func.subs(h, 0)))

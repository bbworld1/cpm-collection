# cpm-collection: Automate the busy work in your textbook

Does your school district use CPM? Does it bore you to have to chug
through dozens of mindless word problems? Do you hate "investigating
functions"? Is it frustrating when you have to manually evaluate
derivatives with the formal definition of a derivative,
when you could just use the power rule?

Fear no more, the CPM Collection is for you! Although the repo is titled
CPM, these scripts help to automate a variety of mindless math problems
by generating all the work shown for you. Instead of spending an hour
plugging numbers into equations, you can simply automate it with these
scripts, allowing you to focus on actually learning important things.

NOTE: These scripts are only for people who understand the concepts and are
bored with being assigned busy work. If you do not understand the math
concepts, do not use these scripts.

## Requirements
- Python3.5+
- pip
- a brain

## List of Scripts
- area_model.py: an area model solver for factoring polynomials.
- inspector.py: "inspects" a function for you (graph, limits, etc.)
- iroc.py: get the derivative of a function at an X value using the
  formal definition of a limit

## Running a Script
0. Clone/download this repository
1. (FIRST TIME ONLY) install dependencies with `pip3 install -r requirements.txt`
2. Run the script with `python3 <scriptname>.py`. The script will print out
   help text describing how to use it; follow the instructions.

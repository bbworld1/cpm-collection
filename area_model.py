#!/usr/bin/env python3
# Yes, kai, i wrote all this myself. That's why the code is ugly AF.

import re
from collections import namedtuple
from tabulate import tabulate

class Polynomial:
    def __init__(self, str_rep):
        self.sign = str_rep[0]
        self.string = str_rep

        if len(str_rep[1:].split("^")) == 1:
            self.power = 1
        else:
            self.power = int(str_rep[1:].split("^")[-1])

        # TODO: we can probably do better than regex match
        pattern = re.compile("[a-z]")

        try:
            self.var = pattern.search(str_rep[1:]).group(0)
            self.coeff = str_rep.split(self.var)[0]
        except AttributeError:
            # No variable, it's a constant
            self.var = "x"
            self.coeff = self.string
            self.power = 0


        if self.coeff == "":
            self.coeff = 1
        else:
            try:
                self.coeff = round(float(self.coeff), 5)
            except ValueError:
                # Coefficient is 1 (not written)
                self.coeff = 1

        self.string = "{}{}^{}".format(self.coeff, self.var, self.power)


def splitpoly(eq):
    tokens = []
    eqp = eq.split("+")
    tokens = []
    eqp = ["+"+e for e in eq.split("+")]
    for token in eqp:
        tokens.extend(["-"+e if e[0] != "+" else e for e in token.split("-")])
    tokens = list(map(Polynomial, tokens))

    return tokens


def solve_column(begin, left):
    """
    Solve one column of an area model.

    Begin is the beginning term as a Polynomial.
    Left is a split Polynomial in list format.

    """
    # TODO: Make this work with a left side with more than 2 terms
    terms = []
    top = Polynomial("+1")
    top.coeff = begin.coeff / left[0].coeff
    top.var = begin.var
    top.power = begin.power - left[0].power
    print("top: {}{}^{}".format(top.coeff, top.var, top.power))
    print("end: {}{}^{}".format(left[-1].coeff, left[-1].var, left[-1].power))

    for c,term in enumerate(left):
        params = ("+" if top.coeff*term.coeff > 0 else "-",
                  abs(top.coeff * term.coeff),
                  top.var,
                  top.power + term.power)
        print(params)
        terms.append(Polynomial("{}{}{}^{}".format(*params)))


    return top,terms


def factor_model_from_string(eq, root):
    """
    Create an area model of this form:

        top
         x^2 + x  - 3
    left________________
    x^2 |_x^4|_x^3|-3x^2|
    -x  |-x^3|-x^2|__-3x|
    +1  |_x^2|___x|___-3|

    Fill out any missing parts, if possible.
    """
    split = splitpoly(eq)
    tokens = split.copy()
    op = split[0].power

    for c,t in enumerate(split):
        for power in range(t.power+1, op):
            tokens.insert(c, Polynomial("0.0x^{}".format(power)))
        op = t.power
    left = splitpoly(root)
    top = []

    # Solver

    #---

    terms = []
    top = []
    topvar = tokens[0]

    terms.append(tokens[0])
    columns = []

    for i in range(len(tokens)-1):
        print("column", i)
        tv2, col = solve_column(topvar, left)
        columns.append(["{}{}^{}".format(t.coeff, t.var, t.power) for t in col])
        top.append("{}{}^{}".format(tv2.coeff, tv2.var, tv2.power))

        topvar = Polynomial("+1")
        topvar.coeff = tokens[i+1].coeff - col[-1].coeff
        topvar.var = col[-1].var
        topvar.power = col[-1].power
        print()

    print()
    print()
    print(tabulate(zip(*columns), top, tablefmt="grid"))


if __name__ == "__main__":
    # Get inputs and solve
    eq = input("Enter the polynomial you want to factor: ")
    l = input("Enter a root in form (x-r): ")
    factor_model_from_string(eq, l)
